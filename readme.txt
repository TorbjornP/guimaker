﻿Title : guimaker
Type : Scilab toolbox
Version : 1.91
Date : 3-May-2015
Author : Torbjørn Pettersen <top@tpett.com>
Description : Tools for easy creation of graphical user interfaces. 
Licence : See license.txt

Installation and usage:
 1) First time installation:
   - copy the directory guimaker to the SCI\contrib
   - from the Scilab Console, run the following commands:
       exec SCI\contrib\guimaker\builder.sce // only for non Windows installations
       exec SCI\contrib\guimaker\loader.sce

 2) Later usage:
   - load toolbox guimaker from the Toolboxes menu in the Scilab Console.

 3) Help and documentation
   - see section "GUI Tools" in the Scilab help browser
   - see section "GUI Tools" in the Scilab demonstrations menu