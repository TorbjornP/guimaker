function varargout=inputui(title_txt,text,values,filename)
  // A single line frontend for <link type="scilab" linkend="scilab.help/x_mdialog">x_mdialog</link>, with some additional functionality.
  // Calling sequence
  //  [varargout]=inputui(title_txt,text[,values][,filename])
  // Parameters
  //  title_txt : title text
  //  text : list() with text labels for each input variable to be retrieved.
  //  values : optional list() with default values for each input variable. 
  //  filename : optional file name for retrieval of default values stored by a previous call to inputui.
  // Description
  //  inputui is a front end for the Scilab function <link type="scilab" linkend="scilab.help/x_mdialog">x_mdialog</link> which only require a single command line.
  //  
  //  Values may be stored between successive calls to inputui within the current Scilab session. 
  //  The values are stored in the TMPDIR directory as a file named [filename] (see the optional fourth input parameter). 
  //  Note that these values are only stored during the current Scilab session.
  //  Use unique values for [filename] in order to store values for different types of calls to inputui.
  //   
  // Examples
  //  [mag,frq,ph]=inputui('Enter sine signal',list('magnitude','frequency','phase'),list(1,10,0));  // Single line call...
  //
  //  // Store values between successive calls to inputui... 
  //  [a,b]=inputui('Edit a and b',list('a','b'),list(1,2),'inputui_demo')  // change the values for a and b and press OK.
  //  [a,b]=inputui('Test',list('a','b'),list(1,2),'inputui_demo') // default values are now retrieved from TMP\inputui_demo
  // See also
  //  menumaker
  //  guimaker
  // Authors
  //  T. Pettersen, top@tpett.com

//  Copyright Torbjørn Pettersen  2008-2011

  if argn(2)>3 then apifun_checktype("inputui",filename,"filename",4,["string"]); end
  if argn(2)>2 then apifun_checktype("inputui",values,"values",3,["list" "constant"]); end
  apifun_checktype("inputui",text,"text",2,["list"]);
  apifun_checktype("inputui",title_txt,"title_txt",1,["string"]);

  if argn(2)<3 | isempty(values) then 
    values=list();
    for i=1:length(text), values(i)=[]; end
  end
  if length(text)<>length(values) then error('Number of text strings does not match number of default values'); end
  if length(values)<>argn(1) then error('Number of output arguments does not match number of default values'); end
    
  if argn(2)<4 then filename=''; end
  if ~isempty(fileinfo(TMPDIR+'\'+filename+'.dat')) then 
    load(TMPDIR+'\'+filename+'.dat'); 
  end

  val=[]; txt=[];
  for i=1:length(values), 
    [m,n]=size(values(i)); 
    if m*n>1 then
      str='[';
      for k=1:m
        for l=1:n
          str=str+sprintf('%g ',values(i)(k,l));
        end
        if k<m then str=str+';'; end;
      end
      str=str+']';
      val=[val;str];
    else
      if type(values(i))==1 then
        if values(i)==[] then val=[val;'[]']; else val=[val;string(values(i))]; end
      else
        val=[val;sprintf('''%s''',values(i))];
      end
    end
    txt=[txt;text(i)]; 
  end

  sig=x_mdialog(title_txt,txt,val);
  
  if ~isempty(sig) then 
    for i=1:length(values), 
      if isempty(sig(i)) then 
        varargout(i)=''; 
      else 
        try
          varargout(i)=evstr(sig(i));
        catch
          tmp=tokens(sig(i));
          if isnum(tmp(1)) then 
            varargout(i)=evstr(sig(i)); 
          else  
            varargout(i)=sprintf('%s',sig(i)); 
          end
        end
      end; 
    end;
  else
    for i=1:length(values), varargout(i)=[]; end;
    return
  end  

  if ~isempty(filename) then
    values=varargout;
    save(TMPDIR+'\'+filename+'.dat',values);
  end 
endfunction
