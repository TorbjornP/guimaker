function [h_menu] = menumaker(varargin)
// Add menus to a figure 
//
// Calling Sequence
//   [h_menu] = menumaker(menus[,callbackFun][,f][,Flag]) 
//   [h_menu] = menumaker(menus) // creates a new figure with menus 
//   [h_menu] = menumaker(menus,callbackFun)  // as above but also with callbackFun('Label') as callback
//
// Parameters
// menus: list defining the menus to be added.
// menu: element in menus where menu=list(Label,callback,arg1,arg1val,....) or menu=list(Label,submenus)
// Label: string with the label name of a menu entry.
// callback: string containing the callback of the menu item. If callback='' then callbackFun will be used.
// argn: optional relevant arguments like Enable,Checked,ForgroundColor,Tag og Visible. See <link type="scilab" linkend="scilab.help/uimenu">uimenu</link>.
// submenus: list defining submenus item in a menu (submenus elements have the same structure as menu)
// callbackFun: optional string with the name of the callback function to be called as callbackFun('Label') if no callback is given for any particular item.
// f: optional figure handle. If empty then f=gcf().
// Flag: optional Flag=1 remove standard menus, Flag=2 remove Graphics menu or Flag3=remove both.
// h_menu: list of handles to the menus. Handles to sub menus are present as lists.
//
// Description
// menumaker is a tool for adding menus to figures and gui's with a minimum of programming. 
//
// The menu structure is defined using nested lists similar to the way 
// gui's are defined in guimaker. See the Parameters section and the examples. 
//
// 
// Examples
// // Example from help uimenu
//  scf();
//  menus=list(list('windows',list('operations',...
//                                 list('new window','show_window()'),...
//                                 list('clear window','delete(gca())'))),...
//             list('quit scilab','exit'),...
//             list('$\LaTeX$',list('$\int_0^\infty\mathrm{e}^{-x^2}\,dx$',''),...
//                             list('$\frac\sqrt{\pi}2$','')),...
//             list('MathML',list('<msup><mn>x</mn><mi>2</mi></msup>',''),...
//                           list('<mrow><msup><mn>a</mn><mi>2</mi></msup><mo>+</mo><msup><mn>b</mn><mi>2</mi></msup><mo>=</mo><msup><mn>c</mn><mi>2</mi></msup></mrow>','')));
//  menumaker(menus,'messagebox');
//    //
//    // Example from the demo section GUI - Uicontrol1
//    scf();
//    menus=list();
//    menus($+1)=list('Application',list('Exit figure','close(gcf())'),...
//    list('Quit Scilab','quit'));
//    menus($+1)=list('Scilab Graphics',list('Launch plot3d','plot3d()'),...
//    list('Launch plot2d','plot2d()'));
//    menus($+1)=list('Menu',list('Sub menu 1','disp(''''Sub menu 1'''')'),...
//    list('Sub menu 2',list('Sub menu 2-1','disp(''''Sub menu 2-1'''')'),...
//                      list('Sub menu 2-2','disp(''''Sub menu 2-2'''')')),...
//    list('Sub menu 3','if get(gcbo,''''checked'''')==''''on'''' then set(gcbo,''''checked'''',''''off''''); else set(gcbo,''''checked'''',''''on''''); end'));
//    menumaker(menus);
//    demo_viewCode('menumaker2.dem.sce');
// See also
//  guimaker
//  inputui
// Authors
//  Torbjørn Pettersen, top@tpett.com
  
//  Copyright Torbjørn Pettersen  2010-2011

apifun_checktype("menumaker",varargin(1),"callbackFun",1,"list");
if argn(2)>1 then apifun_checktype("menumaker",varargin(2),"callbackFun",2,["string" "constant"]); end
if argn(2)>2 then apifun_checktype("menumaker",varargin(3),"f",3,["handle" "constant"]); end
if argn(2)>3 then apifun_checktype("menumaker",varargin(4),"Flag",4,["constant"]); end


f=[]; callbackFun=''; Flag=0; EventTemplate=%f;
if argn(2)>4 then EventTemplate=varargin(5); end
if argn(2)>3 then Flag=varargin(4); end
if argn(2)>2 then f=varargin(3); end
if argn(2)>1 then
    if typeof(varargin(2))=='string' then
        callbackFun=varargin(2);
    else
        f=varargin(2);
        callbackFun='';
    end
end
if argn(2)>0 then 
    menus=varargin(1);
end
if argn(2)==0 then    // demo mode
    f=scf(); callbackFun='disp';
    menus=list();
    menus($+1)=list('Application',list('Exit figure','close(gcf())'),list('Quit Scilab','quit'));
    menus($+1)=list('Scilab Graphics',list('Launch plot3d','plot3d()'),list('Launch plot2d','plot2d()'));
    menus($+1)=list('Menu',list('Sub menu 1',''),...
                           list('Sub menu 2',list('Sub menu 2-1','disp(''''Sub menu 2-1'''')'),...
                                             list('Sub menu 2-2',''))...
                           );
end
if isempty(f) then f=gcf(); end

if Flag==1 | Flag==3 then // Remove Scilab 'standard' menus
    delmenu(f.figure_id, gettext("&File"));
    delmenu(f.figure_id, gettext("&Tools"));
    delmenu(f.figure_id, gettext("&Edit"));
    delmenu(f.figure_id, gettext("&?"));
end
if Flag==2 | Flag==3 then // Disable graphics toolbar
    toolbar(f.figure_id, "off");
end

cbFunTemplate=['function '+callbackFun+'(cmd)';...
               '// Template for a callback event function created by menumaker()';...
               'select cmd,'];

// Menu = list(Label,callback,...) or Menu = list(Label,list(Label 1,'',..),list(Label 2,'',...))
h_menu=list();Parent=f;
for i=1:length(menus),    // for every menu in the menus list
    [h_menu($+1),cbFunEntry]=menumaker_putmenu(Parent,menus(i),callbackFun);
    if ~isempty(cbFunEntry) then
        cbFunTemplate=[cbFunTemplate;cbFunEntry];
    end
end

cbFunTemplate=[cbFunTemplate;'end';'endfunction'];
if EventTemplate then
    fd=mopen(TMPDIR+'/callbackFunTemplate.sci','wt');
    if fd then
        mputl(cbFunTemplate,fd);
        mclose(fd);
        editor(TMPDIR+'/callbackFunTemplate.sci');
    else
        error('Failed to create TMPDIR/callbackFunTemplate.sci.');
    end
end
endfunction

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

function [h_list,cbFunEntry]=menumaker_putmenu(Parent,Menu,callbackFun)
    // Input check: Menu = list(Label,callback,...) or Menu = list(Label,list(Label 1,..),list(...))
    if typeof(Parent)<>'handle' then
        error(sprintf(gettext("%s: Wrong type for input argument #%d: handle expected.\n"),"menumaker_putmenu",1));
    end
    if typeof(Menu)<>'list' then
        error(sprintf(gettext("%s: Wrong type for input argument #%d: list expected.\n"),"menumaker_putmenu",2));
    end
    cmd='h=uimenu(""Parent"",Parent,""Label"",Menu(1)';
    cbFunEntry='';
    if typeof(Menu(2))=='string' then // like Menu=list(Label,'',....) or Menu=list(Label,'callback',...)
        if ~isempty(Menu(2)) then    // an individual callback command is defined
            cmd=cmd+sprintf(',""callback"",""%s""',Menu(2));
        else
            if isempty(callbackFun) then // callback throuh callbackFun - same to template...
                error(sprintf('Needs to have callbackFun defined for menu %s.',Menu(1)));
            end
            cmd=cmd+sprintf(',""callback"",''%s(""""%s"""")''',callbackFun,strsubst(Menu(1),' ','_'));
            cbFunEntry=[cbFunEntry;'    '''+strsubst(Menu(1),' ','_')+'''';..
                        '        disp(''<callback from '+Menu(1)+'>'')'];
        end
        if length(Menu)>2 then
            for i=3:2:length(Menu),
                cmd=cmd+sprintf(',Menu(%i),Menu(%i)',i,i+1);
            end
        end
    end
    cmd=cmd+');';
    execstr(cmd); // create initial label
    if typeof(Menu(2))=='list' then // like Menu=list(Label,list(Label 1,...),list(....))
        Parent=h;
        h_sublist=list();
        for i=2:length(Menu),
            h=menumaker_putmenu(Parent,Menu(i),callbackFun);
            h_sublist($+1)=h;
        end
        h_list=h_sublist;
    else
        h_list=h;
    end
endfunction
