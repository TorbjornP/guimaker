function varargout=guimaker(page,guiProp,DefProp,Flag)
  // Create a graphical user interface with a minimum of programming.
  // Calling Sequence
  //  [val1,val2,...]=guimaker(page,[guiProp],[DefProp],[Flag]) // interactive mode return user input from objects in the gui
  //  sci=guimaker(page,[guiProp],[DefProp],1) // return the Scilab source code required for generation of the gui
  //  links=guimaker(page,[guiProp],[DefProp],2) // create gui and return a list of handles to the gui objects
  //  guimaker() // runs a demo...
  // Parameters
  //  page : a list which defines the gui as a list of lines. Lines are counted from top to bottom of the gui.
  //  line : a line is defined as a list of objects. Empty elements are interpreted as empty places in the line.
  //  object : objects are defined as a list([whf],Style,String,Property1,Value1,Property2,Value2,...) or [] to indicate an empty position in the line.
  //  whf : OPTIONAL vector where whf(1) defines the object width relative to the width of the gui. whf(2) is the object height measured in number of lines. Default whf=[1 1]. Optional whf(3)=1 reduces the length of the object to fit into a frame.
  //  Style : string with the object type {axes | checkbox | edit | frame | image | layer | listbox | popupmenu | pushbutton | radiobutton | slider | spinner | tab | table | text}.
  //  String : string containing the 'string' value of the object.
  //  Property : string with object properties like { BackgroundColor | callback | fontangle | fontsize | fontunits | fontname | ForegroundColor | Horizontalalignment | ListboxTop | Max | Min | SliderStep | String | Tag | Units | Userdata | Value | Verticalalignment}. For axes objects see <link type="scilab" linkend="scilab.help/axes_properties">axes_properties</link>. 
  //  Value : value of the previous object property, see <link type="scilab" linkend="scilab.help/uicontrol">uicontrol</link> for more details.
  //  guiProp : OPTIONAL list(Title,width,height,space,Backgroundcolor) defining gui window properties.
  //  Title : OPTIONAL string with name of the gui figure (default is 'guimaker').
  //  width : OPTIONAL width (points) of the gui window (default is 450).
  //  height : OPTIONAL height (points) of a line (default is 24).
  //  space : OPTIONAL space (points) between lines (first number) and objects on lines (second number) (default is [6 12])
  //  Backgroundcolor : OPTIONAL vector [R,G,B] with background color codes (default is [.9 .9 .9])
  //  DefProp : OPTIONAL list(Property1,Value1,...) with default properties for objects used in the gui.
  //  Flag : OPTIONAL =1: return Scilab code; =2: return list of object handles; ="filename": save/retrieve user input between successive calls to guimaker (values are stored in a file named 'TMPDIR\'+Flag).
  //  links : vector of object handles to each object in the gui (starting with the handle to the gui window and proceeds with objects from the top left to the bottom right position).
  //  val1,val2,... : results {string | value} from objects, except { pushbutton | text | frame }, in the order from the top left to the bottom right position in the gui. If only val1 is present the results are returned as a list.
  // Description
  //  guimaker is a tool for creating a graphical user interface (gui) with
  //  a minimum of programming. 
  //
  //  This is accomplished by describing the layout of the gui using a nested list. 
  //  The input parameter page consists of a list of line elements.
  //  Each line element consists of a list of object elements, present in the line.
  //  Each object element consists of a list of elements describing the properties
  //  of the object (style, string and an arbitrary range of properties 
  //  and property values). 
  //
  //  The layout of the gui is defined by the way lines are counted from the top 
  //  to the bottom of the gui, while objects in a line are counted from left to right.
  //  The objects on a line are distributed evenly in the horizontal direction.
  //  Using the optional object parameter wh one may change the size of 
  //  individual objects in the horizontal and vertical direction.
  //  
  //  guimaker can be used in three different modes: 
  //  <itemizedlist>
  //  <listitem><para>Create a gui - including the event loop:
  //
  //  Called with multiple output arguments 
  //  (<literal>[val1,val2,...]=guimaker(...)</literal>)
  //  the output arguments will be assigned values from the "string" or "value" fields 
  //  of objects present in the gui. The values are assigned to the output 
  //  arguments according to the order the objects occur in the gui, counting objects 
  //  from top left to the bottom right of the gui. 
  //
  //  If only one output argument is given the object values are returned as a list.
  //  Objects with style "pushbutton", "text" or "frame" do not return values.
  //
  //  The gui is kept open until a callback <literal>'OK=%T;'</literal> is 
  //  executed by one of the objects in the GUI. If a callback 
  //  <literal>'CANCEL=%T;'</literal> is executed or if the gui 
  //  window is killed, all output arguments are returned empty.
  //
  //  The user must define an object (usually a pushbutton) with callback set to
  //  <literal>'OK=%T;'</literal> and optionally also an object with callback set 
  //  to <literal>'CANCEL=%T;'</literal> - to implement a Cancel function. 
  //  If the <literal>Flag</literal> input parameter is set to a valid 
  //  file name the object values {string | value } will
  //  be stored in the temporary directory <literal>TMPDIR</literal> under the 
  //  filename <literal>Flag</literal>. 
  //  Initial object values will be read from this file during future calls within 
  //  the current Scilab session.</para></listitem>
  //  <listitem><para>Creation of a gui with no event loop:
  //
  //  If the input argument <literal>Flag</literal> is equal to 2 
  //  guimaker will create the gui and return a vector of handles.
  //  links(1) is the handle to the gui figure window, while link(2:$) are handles to 
  //  each object present in the gui (the handles are listed counting 
  //  from top left to bottom right in the gui).
  //  </para></listitem>
  //  <listitem><para>Create only the source code for a gui, but not the gui itself:
  //
  //  Called with one output parameter and the <literal>Flag</literal> 
  //  input parameter set to 1
  //  guimaker will return the source code for the gui as a string matrix. 
  //  The resulting code can be used and modified independently of guimaker.
  //  </para></listitem>
  //  </itemizedlist>
  // Examples
  //  // Example from http://wiki.scilab.org/howto/guicontrol
  //    page=list(); 
  //    page($+1)=list(list([1 10],'frame','General properties')); 
  //    page($+1)=list(list('text','Particle diameter [nm]'),list('edit','1000'));
  //    page($+1)=list(list('text','Particle density [g/cm3]'),list('edit','1'));
  //    page($+1)=list(list('text','Gas viscosity (0C) u [N*sec/m2]'),list('edit','0.0000172'));
  //    page($+1)=list(list('text','Gas density (0C), 1 atm [kg/m3]'),list('edit','1.29'));
  //    page($+1)=list(list('text','Mean free path (0C), 1 atm [nm]'),list('edit','67'));
  //    page($+1)=list(list('text','Gamma, Cv/Cp'),list('edit','1.4'));
  //    page($+1)=list(list('text','Sutherlands temperature, [K]'),list('edit','110.4'));
  //    page($+1)=list(list('text','Temperature, [K]'),list('edit','300'));
  //    page($+1)=list(list('text','Volume flow rate [standard l/min]'),list('edit','0.5'));
  //    page($+1)=list(list([1 4],'frame','Telescope properties'));
  //    page($+1)=list(list('text','Orifice diameters [mm]'),list('edit','1,1,1,1,1'));
  //    page($+1)=list(list('text','Tube diameters [mm]'),list('edit','100,100,100,100,100'));
  //    page($+1)=list(list('text','Input diameters [mm]'),list('edit','201325'));
  //    page($+1)=list(list(2),list('pushbutton','Stop','callback','OK=%t'),list(2));
  //    [dp,rhop,mu0,rhog0,lambda0,CvCp,S,T,mdot,df,ds,pin]=guimaker(page,list('Aerodynamic Lens Design'))
  //
  //  // Another example from http://wiki.scilab.org/howto/guicontrol
  //    pg=list();
  //    pg($+1)=list(list('text','exposure time','Horizontalalignment','center'));
  //    pg($+1)=list(list('slider','','Min',0,'Max',100,'Value',5));
  //    pg($+1)=list(list('text','arevscale','Horizontalalignment','center'));
  //    pg($+1)=list(list('slider','','Min',0,'Max',100,'Value',50));
  //    pg($+1)=list(list([1 2],'pushbutton','STOP','callback','OK=%t'),list(2,'radiobutton','bin x2'));
  //    pg($+1)=list(list(1),list(2,'radiobutton','free/trig'));
  //    [hexp,hbri,hbin,htrig]=guimaker(pg,list('objfigure1',220,[],[10 10]))
  //    
  //  // If Flag is set to a valid file name the results are stored in TMPDIR/Flag...
  //    [output]=guimaker(pg,list('Change some values and press STOP',220,[],[10 10]),[],'tmp_file')
  //  
  //  // ... and subsequent calls will read the initial values from TMPDIR/Flag. 
  //    [output]=guimaker(pg,list('...remembers previous values.',220,[],[10 10]),[],'tmp_file')
  //  
  //  // Source code generation:
  //    source_code = guimaker(pg,list('objfigure1',220),[],1) // The code may be used as a template. 
  // fn=TMPDIR+filesep()+'guitemplate.sce';
  // f=mopen(fn,'w'); mputl(source_code,f); mclose(f);
  // editor(fn); // Use this code as basis for more advanced programming
  //
  //  // Create gui and return a list of handles to objects in the gui:
  //    h=guimaker(pg,list('objfigure1',220),[],2)  // returns a vector of object handles
  //  // The object handles may be used in an external event control loop. 
  //  //
  //  // -----------------------------------------
  //  // See the guimaker demos for more examples.
  //  // -----------------------------------------
  // See also
  //  inputui
  //  menumaker
  // Bibliography
  //  http://wiki.scilab.org/howto/guicontrol ; provides more information on gui's in Scilab.
  // Authors
  // T. Pettersen, top@tpett.com

  //  Copyright Torbjørn Pettersen  2008-2015

  // Check input arguments...
  if argn(2)<4 | isempty(Flag) then Flag=[]; ShowCode=%f; ReturnValues=%t; FileName=[]; Values=[]; end
  if argn(2)<3 | isempty(DefProp) then 
//    DefProp=list('Backgroundcolor',[1 1 1]*.9,'Horizontalalignment','left','Fontsize',12); 
    DefProp=list('Horizontalalignment','left','Fontsize',12); 
  end
  if argn(2)<2 | isempty(guiProp) then guiProp=list(); end
  if argn(2)<1 | isempty(page) then // start demo...
    demo=[
    'page=list();';..
    'page($+1)=list(list([1,2],''frame'',''A frame...''));';..
    'page($+1)=list(list(''text'',''Enter input''),list(''edit'',''in this field''));';..
    'page($+1)=list(list(''radiobutton'',''radiobutton''),list(''checkbox'',''checkbox''));';..
    'page($+1)=list(list(''slider'','''',''Min'',0,''Max'',100,''SliderStep'',[1 10],''Value'',35),list(''popupmenu'',''option one|two|three|four|five''));';..
    'page($+1)=list(list(1),list(''pushbutton'',''OK'',''callback'',''OK=%T''),list(1),list(''pushbutton'',''Cancel'',''callback'',''CANCEL=%T''),list(1));'];
    help guimaker
    execstr(demo);
    printf('%s\n',[demo; 'guimaker(page)']);
  end

  if length(guiProp)<1 | isempty(guiProp(1)) then Title='guimaker'; else Title=guiProp(1); end
  if length(guiProp)<2 | isempty(guiProp(2)) then W=450; else W=guiProp(2); end
  if length(guiProp)<3 | isempty(guiProp(3)) then h=24; else h=guiProp(3); end
  if length(guiProp)<4 | isempty(guiProp(4)) then dh=6; dw=12; else dhw=guiProp(4); dh=dhw(1); dw=dhw(2); end
  if length(guiProp)<5 | isempty(guiProp(5)) then Backgroundcolor=[.9 .9 .9]; else Backgroundcolor=guiProp(5); end

  if type(Flag)==10 then    // a filename is given
    FileName=Flag; ShowCode=%f; ReturnValues=%t;  // run interactively
    if ~isempty(fileinfo(TMPDIR+filesep()+FileName)) then // if old Values exists
      load(TMPDIR+filesep()+FileName,'Values');           // load them
    else
      Values=[];
    end
  elseif Flag==1 then      // return code
    ShowCode=%t; ReturnValues=%f; Values=[];
  elseif Flag==2 then       // return handles to gui objects.
    ShowCode=%f; ReturnValues=%f; Values=[];
  end

  // Final input check.
  apifun_checktype("guimaker",page,"page",1,"list");
  apifun_checktype("guimaker",guiProp,"guiProp",2,["list" "constant"]);
  apifun_checktype("guimaker",DefProp,"DefProp",3,["list" "constant"]);
  apifun_checktype("guimaker",Flag,"Flag",4,["constant" "string"]);
  if ~inlist(DefProp,'backgroundcolor') then
      DefProp($+1)="Backgroundcolor"; DefProp($+1)=[.9 .9 .9];
  end
// Various parameters used in this messy code.
//  W - gui width in points
//  H - gui height in points
//  nlines - number of lines in gui
// in loop
//  Y - y coordinate for the current line 
//  X - x coordinate for the current object 
//  i - line number
//  nobj - number of objects in current line
//  

  // Check output arguments and figure out what to do...
  if ReturnValues then   // ... need to return values from input fields
    h=guimaker(page,guiProp,DefProp,2); // create gui and get hold of the handles

    if ~isempty(Values) then // values loaded from previous run...
      j=1;
      for i=2:length(h),      // update the values of all objects...
        stl=convstr(get(h(i),'style'));
        if find(stl==['edit']) then
          if typeof(Values(j))=='string' then
            set(h(i),'string',Values(j));
          else
            set(h(i),'string',sci2exp(Values(j))); 
          end
          j=j+1;
        elseif isempty(find(stl==['frame','pushbutton','text'])) then
          set(h(i),'value',Values(j)); j=j+1;
        end
      end
    end
    
    // place a unique tag on the gui window 
    guiTag=sprintf('guimaker%f',rand(1)); set(h(1),'Tag',guiTag); 
    OK=%F; CANCEL=%F; 
    out=list(); for i=1:argn(1), out(i)=[]; end; // prepare for an "emergency exit"
    while ~OK & ~CANCEL     // event loop
      sleep(50); 
      gui=findobj('Tag',guiTag);
      if isempty(gui) then  // "emergency exit"
        error(sprintf(gettext("%s: The graphical user interface window was killed by the user.'),"guimaker")); 
      end 
    end
    if OK then
      OutputObj=list();               // which objects contain output values?
      for i=2:length(h),      
        ObjType=get(h(i),'Style');
        if ObjType~='pushbutton' & ObjType~='text' & ObjType~='frame' then
          OutputObj($+1)=h(i);        // everyone does except the three types above...
        end
      end
      for i=1:length(OutputObj),            // For every output object...

        ObjType=get(OutputObj(i),'style');  // read style...
        if ObjType=='edit' then    // for style='edit'
          str=get(OutputObj(i),'string');       // read the string property
          if isnum(str) then    // if str represents a number
            out(i)=evstr(str);  // evaluate to number
          else                  // else
            out(i)=str;         // return the string
          end
        elseif ObjType=='table' then // for style='table'
          out(i)=get(OutputObj(i),'string');    // sorry - can't remember rows and columns in table. 
        else
          out(i)=get(OutputObj(i),'value');             // other styles - read the value property
        end
      end
    end
    if ~isempty(FileName) then            // if a filename was given
      Values=out;                         // store output values there
      save(TMPDIR+filesep()+FileName,Values);   // for later runs.
    end
    if argn(1)>1 then   // if more than one output argument
      varargout=out;    // return output to each argument
    else                // otherwise return the output as a list
      varargout=list(); varargout(1)=out; 
    end;
    close(h(1));      // close gui window - and exit.
  else                // ... or just create the gui and return handles to objects
    vn=getversion('scilab'); vn=vn(1)+vn(2)/10;
    if vn<5.2 then
      Offset=40;                   // offset due to the figure format in Scilab 5.1
    elseif vn<5.5
      Offset=-80;                 // offset due to figure format in Scilab 5.2
    else 
      Offset=-15;                 // works for Scilab 5.5
    end
    tbMargin=2*dh;                  // top and bottom margins on page 
    lrMargin=dw;                  // lef and right margins on page 

    nlines=0; ExtraLines=0;   // Figure out how tall the gui will be in number of lines...
    for i=1:length(page),     // for every line...
      line=page(i);
      lineHeight=1;
      for k=1:length(line),   // for every object on a line
        if typeof(line(k)(1))=='constant' & size(line(k)(1),'*')>1 then
          lineHeight=max(line(k)(1)(2),lineHeight); // store the tallest object...
        end
      end
      if lineHeight>1 & lineHeight>ExtraLines then 
        ExtraLines=lineHeight; 
      end
      nlines=nlines+1;
      if ExtraLines>0 then ExtraLines=ExtraLines-1; end
    end
    nlines=nlines+ExtraLines;   // height of gui in number of lines...
    H=Offset+(nlines+1)*(h+dh)+2*tbMargin+h;  // height in points of the gui windows
    Y=(H-Offset)-(tbMargin+dh+h);          // Y location for the first line in the gui

    links=[];               // list with handles to objects that will be created
    figNo=max(winsid())+1;  // next available figure number
    if ShowCode then
      source=sprintf('fig=figure(%i,''figure_name'',''%s'',''position'',[0 0 %i %i],''Backgroundcolor'',[%f %f %f],""infobar_visible"", ""off"", ""toolbar_visible"", ""off"", ""dockable"", ""off"", ""menubar"", ""none"");',figNo,Title,W,H,Backgroundcolor(1),Backgroundcolor(2),Backgroundcolor(3)); 
  //    source=[source;'toolbar(fig.figure_id,""off"");']; 
// v1.6: Bug fix thanks to Ihor Rokach (rokach@tu.kielce.pl) [...
  //    source=[source;'delmenu(fig.figure_id,gettext(""&File"")); delmenu(fig.figure_id,gettext(""&Tools""));'];
  //    source=[source;'delmenu(fig.figure_id,gettext(""&Edit"")); delmenu(fig.figure_id,gettext(""&?""));'];
// v1.6: Bug fix thanks to Ihor Rokach (rokach@tu.kielce.pl) ...]
      itt=1; // handle counter...
    else
      fig=figure(figNo,'figure_name',Title,'position',[0 0 W H],'BackgroundColor',Backgroundcolor,"infobar_visible", "off", "toolbar_visible", "off", "dockable", "off", "menubar", "none");
      links(1)=fig; 
  //    toolbar(fig.figure_id,"off"); // hide the toolbar
// v1.6: Bug fix thanks to Ihor Rokach (rokach@tu.kielce.pl) [...
  //    delmenu(fig.figure_id,gettext("&File")); delmenu(fig.figure_id,gettext("&Tools")); // get rid of the
  //    delmenu(fig.figure_id,gettext("&Edit")); delmenu(fig.figure_id,gettext("&?"));     // default menus
// v1.6: Bug fix thanks to Ihor Rokach (rokach@tu.kielce.pl) ...]
    end
    ValuesCounter=1;        // counter used in case Values is defined.
    nValues=length(Values); // number of Values...
    for i=1:length(page)    // for every line in a page
      line=page(i);         // get current line
      nobj=length(line);    // count # of objects in current line
      x=ones(1,nobj); nl=ones(1,nobj); df=zeros(1,nobj);
      for k=1:nobj,                                   // for every object in the line
        if typeof(line(k)(1))=='constant' then        // figure out if we have special 
          if size(line(k)(1),'*')>1 then             // width requirements for 
            x(k)=line(k)(1)(1); nl(k)=line(k)(1)(2);  // each object in the line
          else
            x(k)=line(k)(1);
          end
          if length(line(k)(1))==3 then df(k)=line(k)(1)(3); end    // if we are inside a frame 
          if length(line(k))>1 then line(k)=list(line(k)(2:$)); else line(k)=list(); end
        end
      end

      X=lrMargin; 
      w = (W-(nobj+1)*dw-lrMargin/2)*x/sum(x); 
      for j=1:nobj        // for every object in current line
        obj=line(j);      // pick the object
        if ~isempty(obj) then // and create it if it exists
          if isempty(Values) | (obj(1)=='text' | obj(1)=='pushbutton' | obj(1)=='frame') then
            xywh=[X+df(j)*dw/2 Y-(h+dh)*nl(j) w(j)-df(j)*dw h+(h+dh)*(nl(j)-1)];
            xywh_frame=[X-dw/2+df(j)*dw/2 Y-dh-(h+dh)*nl(j) w(j)+dw-df(j)*dw (h+dh)*nl(j)-h/2];
            if obj(1)=='frame' then   // allow for some extra room for a frame - and add heading..
              cmd=putguiobj(obj(1),'',xywh_frame,list(obj(3:$)),DefProp);
              if ~inlist(obj,'horizontalalignment') then obj($+1)='horizontalalignment'; obj($+1)='center'; end
              cmd=[cmd;putguiobj('text',obj(2),[X+h/2 Y-dh/2-(h+dh) h/3*length(obj(2)) h],list(obj(3:$)),DefProp)]; // frame legend
            elseif obj(1)=='axes' then // special treatment for axes....
              cmd=['drawlater(); newaxes(); axh=gca(); // axes code'];
              xa=X/W; ya=(H-Offset-Y)/(H-Offset); dxa=w(j)/W;  dya=ya+(nl(j)-1)*(h+dh)/H; // something wrong here for multiple axes...
              cmd=[cmd;sprintf('axh.axes_bounds=[%f %f %f %f]; // axes code',xa,ya-(h+dh)/H,dxa,dya)];
              for ii=2:2:length(obj),
                newitem=sprintf("axh.%s=",obj(ii));
                if type(obj(ii+1))==10 then // string value
                  newitem=newitem+sprintf("""%s""; // axes code",obj(ii+1));
                else  // numeric value
                  newitem=newitem+sprintf('%s',string(obj(ii+1)));
                end
                cmd=[cmd;newitem]; 
              end              
              cmd=[cmd;"drawnow(); // axes code"];
            else  // all other objects are simply created by
              cmd=putguiobj(obj(1),obj(2),xywh,list(obj(3:$)),DefProp);
            end
          else	// Values given or not 'text','pushbutton' or 'frame'
            if ValuesCounter>nValues then 
              warning('Too few Values stored in '+FileName+'... check code'); 
              ValuesCounter=nValues; 
            end
            cmd=putguiobj(obj(1),obj(2),xywh,list(obj(3:$)),DefProp,Values(ValuesCounter));
            ValuesCounter=ValuesCounter+1;
          end  // end if: 
          if ShowCode then
            for i=1:size(cmd,'*'),		// for every command
              if isempty(strstr(cmd(i),'// axes code')) then // check for not an axes object
                source=[source;sprintf('h(%i)=%s;',itt,cmd(i))];  // create uicontrol objects
                itt=itt+1;	// increase handle counter.
              else
                source=[source;cmd(i)];  // axes objects don't return handles
              end
            end
          else
            for i=1:size(cmd,'*'), 		// for every command
              if isempty(strstr(cmd(i),'// axes code')) then // if not an axes object
                links($+1)=evstr(cmd(i));   // create the uicontrol objects
              else    // since axes objects don't return handles
                execstr(cmd(i));	// just execute the commands
              end
            end
          end	// end if Showcode
        end		// end if: "and create it if it exists"
        X=X+w(j)+dw;         // advance to the next x position
      end	// end for j: every object in current line
      Y=Y-(dh+h);           // advance to next y position
    end		// end for i: every line in a page
    if ShowCode then	// return source code
      varargout=list(source);
    else				// return handles to gui objects
      varargout=list(links);
    end
  end		// end if: the figure out what to do section...
endfunction
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
function txt=putguiobj(Style,String,XYwh,Prop,DefProp,DefValue)
  // Style : string - type of uicontrol
  // String : string - the 'string' value of the object
  // XYwh : 4x1 vector - position coordinates
  // Prop : optional list(Prop1,Val1,Prop2,Val2...) - properties for uicontrol which overrides DefProp
  // DefProp : optional list(Prop1,Val1,Prop2,Val2) - default properties for objects in this gui
  // DefValue : optional value 'string' or 'value' overriding any in Prop.
  // txt : text string with uicontrol calling sequence to create the object.

  if argn(2)<6 then DefValue=[]; end
  if argn(2)<5 then DefProp=[]; end
  if argn(2)<4 then Prop=[]; end;

  SetValue=%t;
  if ~isempty(DefValue) then
    if Style=='edit' then
      String=string(DefValue);
    elseif Style=='radiobutton' | Style=='checkbox' | Style=='slider' | Style=='listbox' | Style=='popupmenu' | Style=='spinner' | Style=='table' then
      for i=1:2:length(Prop),
        if convstr(Prop(i),'l')=='value' then Prop(i+1)=DefValue; SetValue=%f; end
      end
      if SetValue then Prop($+1)='value'; Prop($+1)=DefValue; end
    end
  end
  
  txt='uicontrol(fig,""Position"",'+sprintf('[%i %i %i %i]',XYwh)+',""Style"",""'+Style+'""';
  if ~isempty(String) & Style<>'slider' & Style<>'frame' & Style<>'table' then 
    txt=txt+',""string"",""'+String+'""'; 
  end
  if ~isempty(String) & Style=='table' then
    txt=txt+',""string"",'+sci2exp(String);
  end
  if Style=='pushbutton' then     // Special treatment for pushbutton's
    if ~inlist(Prop,'horizontalalignment') then 
      Prop($+1)='Horizontalalignment'; Prop($+1)='center';  
    end
  end

  if  Style=='frame' then
      if ~inlist(Prop,'relief') then 
        Prop($+1)='Relief'; Prop($+1)="groove";  
      end
  end

  for i=1:2:length(DefProp),
    if ~inlist(Prop,DefProp(i)) then
        if (convstr(Style)=="pushbutton") then 
            if (convstr(DefProp(i))<>"backgroundcolor") then
                Prop($+1)=DefProp(i); Prop($+1)=DefProp(i+1);
            end
        else
            Prop($+1)=DefProp(i); Prop($+1)=DefProp(i+1);
        end
    end
  end
    
  for k=1:2:length(Prop), 
    txt=txt+',""'+Prop(k)+'""';
    if typeof(Prop(k+1))=='string' then
      Prop(k+1)=strsubst(Prop(k+1),'\','\\'); // in 'string' parameters.
      Prop(k+1)=strsubst(Prop(k+1),'[','\['); // to avoid tcl error message
      Prop(k+1)=strsubst(Prop(k+1),']','\]'); // in case [] or \ is used
      txt=txt+',""'+Prop(k+1)+'""';
    elseif length(Prop(k+1))>1 then
      txt=txt+',['+sprintf('%f ',Prop(k+1)(:))+']';
    else
      txt=txt+','+string(Prop(k+1));
    end
  end
  txt=txt+')'; 
endfunction
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
function ind=inlist(lst,tok)
  // Find object in a list and return index to the first location if found.
  // Case insensitive for strings.
  ind=%f;
  for i=1:length(lst),
    if type(tok)==10 & type(lst(i))==10 then
      if convstr(lst(i))==convstr(tok) then ind=i; break; end
    else
      if lst(i)==tok then ind=i; break; end
    end
  end
endfunction

