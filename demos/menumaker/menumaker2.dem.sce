mode(-1)
//
// Demo of menumaker.sci
//
// Example from the demo section GUI - Uicontrol1
scf();
menus=list();
menus($+1)=list('Application',list('Exit figure','close(gcf())'),...
list('Quit Scilab','quit'));
menus($+1)=list('Scilab Graphics',list('Launch plot3d','plot3d()'),...
list('Launch plot2d','plot2d()'));
menus($+1)=list('Menu',list('Sub menu 1','disp(''''Sub menu 1'''')'),...
list('Sub menu 2',list('Sub menu 2-1','disp(''''Sub menu 2-1'''')'),...
                  list('Sub menu 2-2','disp(''''Sub menu 2-2'''')')),...
list('Sub menu 3','if get(gcbo,''''checked'''')==''''on'''' then set(gcbo,''''checked'''',''''off''''); else set(gcbo,''''checked'''',''''on''''); end'));
menumaker(menus);
demo_viewCode('menumaker2.dem.sce');
//========= E N D === O F === D E M O =========//
