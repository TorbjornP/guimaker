mode(-1)
// Demo of guimaker and menumaker
// Compare this example with the original coding based on uicontrol objects in
// SCI\modules\gui\demos\uicontrol.dem.sci

pg=list();
pg($+1)=list(list('pushbutton','Quit demonstration','callback','uicntrl1(''''Quit demonstration'''')'));
pg($+1)=list(list(2,'popupmenu','item1|item2|item3|item4','tag','popupmenu','callback','uicntrl1(''''popupmenu'''')'),...
             list('pushbutton','Display selection','tag','Display selection','callback','uicntrl1(''''Display selection'''')'));
pg($+1)=list(list([2 3],'frame','Slider demo'),...
             list([1 6],'listbox','red|green|blue|yellow','tag','listbox','backgroundcolor',[1 1 1]));
pg($+1)=list(list([2 1 1],'slider','','value',50,'min',0,'max',100,'tag','slider','callback','uicntrl1(''''slider'''')'),list(1));
pg($+1)=list(list([2 1 1],'text','Slider value: 50','tag','slider_text'),list(1));
pg($+1)=list(list([2 3],'frame','Color list edition'),list(1));
pg($+1)=list(list([2 1 1],'edit','<Enter color name here>','tag','ColorEdit'),list(1));
pg($+1)=list(list([2 1 1],'pushbutton','Add color in listbox','tag','AddColor','callback','uicntrl1(''''AddColor'''')'),list(1));
h=guimaker(pg,list('Uicontrols demo',300),[],2); // setting up the gui with guimaker.

// Setting up menus using menumaker
menus=list();
menus($+1)=list('Application',list('Exit figure','close(gcf())'),...
                              list('Quit Scilab','quit'));
menus($+1)=list('Scilab Graphics',list('Launch plot3d','scf();plot3d()'),...
                                  list('Launch plot2d','scf();plot2d()'));
menus($+1)=list('Menu',list('Sub menu 1',''),...
                       list('Sub menu 2',list('Sub menu 2-1','disp(''''Sub menu 2-1'''')'),...
                                         list('Sub menu 2-2','')),...
                       list('Sub menu 3','if get(gcbo,''''checked'''')==''''on'''' then set(gcbo,''''checked'''',''''off''''); else set(gcbo,''''checked'''',''''on''''); end'));
menumaker(menus,'uicntrl1',h(1));
demo_viewCode("uicontrols demo1.dem.sce");
// Eventfunction for gui and menus.
function uicntrl1(cmd)
    select cmd
    case 'Quit demonstration',
        close(gcf());
    case 'popupmenu'
        opts=get(gcbo,'string');
        messagebox(sprintf('You selected ''%s''',opts(get(gcbo,'value'))),'popupmenu selection','info');
    case 'Display selection'
        lb=findobj('tag','listbox');
        if isempty(get(lb,'value')) then
            messagebox('No color selected','Selected colors','info');
        else
            clrs=get(lb,'string'); sel=get(lb,'value');
            msg='Selected colors are: ';
            for i=1:length(sel), msg=msg+clrs(sel(i))+' '; end
            messagebox(msg,'Selected colors','info')
        end
    case 'slider'
        set(findobj('tag','slider_text'),'string',sprintf('Slider value: %i',get(gcbo,'value')));
    case 'AddColor',
        NewColor=get(findobj('tag','ColorEdit'),'string');
        if NewColor=='<Enter color name here>' then
            messagebox('Please enter a color name first','Error','error');
        else
            Colors=get(findobj('tag','listbox'),'string');
            Colors($+1)=NewColor;
            set(findobj('tag','listbox'),'string',Colors);
        end
    case 'Sub_menu_1',
        messagebox('Sub menu 1','Menu selections','info');
    case 'Sub_menu_2-2',
        messagebox('Sub menu 2-2','Menu selections','info');        
    end
endfunction