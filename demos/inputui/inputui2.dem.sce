mode(1)
//
// Demo of inputui.sci
//
// Store values between successive calls to inputui...
[a,b]=inputui('Edit a and b',list('a','b'),list(1,2),'inputui_demo')  // change the values for a and b and press OK.
halt;
[a,b]=inputui('Test',list('a','b'),list(1,2),'inputui_demo') // default values are now retrieved from TMP\inputui_demo
//========= E N D === O F === D E M O =========//
