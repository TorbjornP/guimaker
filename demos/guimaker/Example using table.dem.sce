mode(-1)
// Example using a table
// If you edit items in the table before pressing Ok they are not 
// returned properly to the console. This seems to be a scilab bug.
params = [" " "Country" "Population [Mh]" "Temp.[°C]" ];
towns = ["Mexico city" "Paris" "Tokyo" "Singapore"]';
country = ["Mexico" "France" "Japan" "Singapore"]';
pop  = string([22.41 11.77 33.41 4.24]');
temp = string([26 19 22 17]');
table = [params; [ towns country pop temp ]];

pg=list();
pg($+1)=list(list([3 4],'table',table),list('pushbutton','Ok','callback','OK=%t'));
pg($+1)=list(list([3 1]),list('pushbutton','Stop','callback','CANCEL=%t'));
a=guimaker(pg)
mode(-1)
