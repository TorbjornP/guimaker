mode(1)
// Example from http://wiki.scilab.org/howto/guicontrol
page=list();
page($+1)=list(list([1 10],'frame','General properties'));
page($+1)=list(list('text','Particle diameter [nm]'),list('edit','1000'));
page($+1)=list(list('text','Particle density [g/cm3]'),list('edit','1'));
page($+1)=list(list('text','Gas viscosity (0C) u [N*sec/m2]'),list('edit','0.0000172'));
page($+1)=list(list('text','Gas density (0C), 1 atm [kg/m3]'),list('edit','1.29'));
page($+1)=list(list('text','Mean free path (0C), 1 atm [nm]'),list('edit','67'));
page($+1)=list(list('text','Gamma, Cv/Cp'),list('edit','1.4'));
page($+1)=list(list('text','Sutherlands temperature, [K]'),list('edit','110.4'));
page($+1)=list(list('text','Temperature, [K]'),list('edit','300'));
page($+1)=list(list('text','Volume flow rate [standard l/min]'),list('edit','0.5'));
page($+1)=list(list([1 4],'frame','Telescope properties'));
page($+1)=list(list('text','Orifice diameters [mm]'),list('edit','1,1,1,1,1'));
page($+1)=list(list('text','Tube diameters [mm]'),list('edit','100,100,100,100,100'));
page($+1)=list(list('text','Input diameters [mm]'),list('edit','201325'));
page($+1)=list(list(2),list('pushbutton','Stop','callback','OK=%t'),list(2));
[dp,rhop,mu0,rhog0,lambda0,CvCp,S,T,mdot,df,ds,pin]=guimaker(page,list('Aerodynamic Lens Design'))
