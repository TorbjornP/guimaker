mode(-1)
// gui example 1
pg=list();
pg($+1)=list(list('pushbutton','Quit demonstration','callback','close(gcf())'));
pg($+1)=list(list('popupmenu','item1|item2|item3|item4','tag','popm','callback','cb(''''popm'''')'),..
              list('pushbutton','Display selection','callback','cb(''''Display selection'''')'));
pg($+1)=list(list([1 3],'frame','Slider demo'),...
             list([1 6],'listbox','red|green|blue|yellow','tag','listbx','backgroundcolor',[1 1 1]));
pg($+1)=list(list('slider','','Min',0,'Max',100,'Value',50,'tag','slider','callback','cb(''''slider'''')'),...
             list(1));
pg($+1)=list(list('text','Slider value: 50','tag','sl_txt','horizontalalignment','center'),list(1));
pg($+1)=list(list([1 3],'frame','Colors list edition'),list(1));
pg($+1)=list(list('edit','<Enter color name here>','backgroundcolor',[1 1 1],'tag','new_item'),list(1));
pg($+1)=list(list(0.25),list(1,'pushbutton','Add color in listbox','callback','cb(''''pb_add'''')'),list(1.75));
guimaker(pg,list('Uicontrols demo',350),[],2); // Create gui with guimaker

menus=list();
menus($+1)=list('Application',list('Exit figure','close(gcf())'),list('Quit Scilab','quit'));
menus($+1)=list('Scilab Graphics',list('Launch plot3d','scf();plot3d()'),...
                                  list('Launch plot2d','scf();plot2d()'));
menus($+1)=list('Menu',list('Sub menu 1','messagebox(''''Sub menu 1'''')'),...
                       list('Sub menu 2',list('Sub menu 2-1','messagebox(''''Sub menu 2-1'''')'),...
                                         list('Sub menu 2-2','messagebox(''''Sub menu 2-2'''')')),...
                       list('Sub menu 3','if get(gcbo,''''checked'''')==''''on'''' then set(gcbo,''''checked'''',''''off''''); else set(gcbo,''''checked'''',''''on''''); end'));
menumaker(menus); // create menus with menumaker
demo_viewCode("Uicontrols demo 1.dem.sce");

// Define callback function
function cb(cmd)
  // callback function for gui example 1
  h_lb=findobj('tag','listbx');
  select cmd
    case "slider",
      h_sl=findobj('tag','slider');
      h_sl_txt=findobj('tag','sl_txt');
      set(h_sl_txt,'string','Slider value:'+string(get(h_sl,'value')));
    case "popm",
        h=findobj('tag','popm'); 
        opts=get(h,'string'); 
        messagebox('You selected '''+opts(get(h,'value'))+'''.','popupmenu selection','info');
    case "Display selection"
      selected=get(h_lb,'value'); 
      if isempty(selected) then
          messagebox('No color selected.','Selected colors','info');
      else
          options=get(h_lb,'string');
          txt=sprintf('%s,',options(selected)'); txt=part(txt,1:length(txt)-1);
          messagebox('Selected colors are:'+txt+'.','Selected colors','info');
      end
    case "pb_add"
        h=findobj('tag','new_item');
        if get(h,'string')=='<Enter color name here>' then
            messagebox('Add a new color','Colors list edition','info');
        else
            set(h_lb,'string',[get(h_lb,'string'),get(h,'string')]);
        end
  end
endfunction
mode(-1)
