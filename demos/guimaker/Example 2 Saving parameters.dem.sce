mode(1)
// Another example from http://wiki.scilab.org/howto/guicontrol
pg=list();
pg($+1)=list(list('text','exposure time','Horizontalalignment','center'));
pg($+1)=list(list('slider','','Min',0,'Max',100,'Value',5));
pg($+1)=list(list('text','arevscale','Horizontalalignment','center'));
pg($+1)=list(list('slider','','Min',0,'Max',100,'Value',50));
pg($+1)=list(list([2 2],'pushbutton','STOP','callback','OK=%t'),list(2,'radiobutton','bin x2'));
pg($+1)=list(list(2),list(2,'radiobutton','free/trig'));
 
// If Flag is set to a valid file name the results are stored in TMPDIR/Flag...
[output]=guimaker(pg,list('Change some values and press STOP',220,[],[10 10]),[],'tmp_file')
halt()   // Press return to continue
 
// ... and subsequent calls will read the initial values from TMPDIR/Flag.
[output]=guimaker(pg,list('...remembers previous values.',220,[],[10 10]),[],'tmp_file')
