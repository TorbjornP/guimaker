//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA
// Copyright (C) 2009 - DIGITEO - Vincent COUVERT
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

// Modified for demonstration of guimaker. 
// Torbjørn Pettersen, 2014. 

// Callback functions
function act_color_in_list(flag)
    listbox_handle = get("colors_listbox");
    new_color = get(get("colors_edit"), "String");

    if new_color==gettext("<Enter color name here>") | isempty(new_color) then
        messagebox(gettext("Please enter a color name first."), gettext("Error"), "error", "modal");
        return
    end
    colors = get(listbox_handle, "String");
    if (flag==1) then
        colors = [colors  new_color];
    else
        f = find(colors<>new_color);
        if size(f, "*") == size(colors, "*") then
            messagebox(gettext("Unknown color."), gettext("Error"), "error", "modal");
            return
        end
        colors = colors(f);
    end
    set(listbox_handle, "String", strcat(colors,"|"));
endfunction

function act_selected_colors(flag)
    listbox_handle = get("colors_listbox");
    colors = get(listbox_handle, "String");
    indices = get(listbox_handle, "Value");

    if isempty(indices) then
        msg = gettext("No color selected.");
        messagebox(msg, gettext("Color selection"), "info", "modal");
        return
    end
    if (flag==1) then
        msg = msprintf(gettext("Selected colors are: %s."), strcat(colors(indices), ", "));
    else
        msg = msprintf(gettext("Deleted colors are: %s."), strcat(colors(indices), ", "));
        s = size(colors, "*");
        colors = colors(setdiff(1:s,indices));
        set(listbox_handle, "String", strcat(colors,"|"));
    end
    messagebox(msg, gettext("Color selection"), "info", "modal");
endfunction

function menu_callback()
    msg = msprintf(gettext("You clicked on menu ''%s''."), get(gcbo, "Label"));
    messagebox(msg, gettext("Selected menu"), "info", "modal");
endfunction

function checked_menu_callback()
    if get(gcbo, "Checked")=="on" then
        state = gettext("checked");
    else
        state = gettext("unchecked");
    end
    msg = msprintf(gettext("Menu ''%s'' is %s."), get(gcbo, "Label"), state);
    messagebox(msg, gettext("Menu status"), "info", "modal");
endfunction

function slider_update()
    sl = get("demo_slider");
    txt = get("slider_text");
    set(txt, "String", gettext("Slider value: ") + string(get(sl, "Value")));
endfunction

function popupmenu_callback()
    pop = get("popupmenu_demo");
    items = get(pop, "String");
    selected = get(pop, "Value");

    msg = msprintf(gettext("You selected ''%s''."), items(selected));
    messagebox(msg, gettext("Popupmenu selection"), "info", "modal");
endfunction

function close_uicontrols_demo()
    delete(get("uicontrols_demo_figure"));
endfunction

function reset_uicontrols_demo()
    findChildren = get("popupmenu_demo");
    findChildren.value = [];
    findChildren = get("demo_slider");
    findChildren.value = 50;
    slider_update();
    findChildren = get("colors_edit");
    findChildren.string = gettext("<Enter color name here>");
    findChildren = get("colors_listbox");
    findChildren.string = tokens(initial_colors,"|")';
endfunction


function exit_scilab()
    msg = gettext("Do you really want to quit Scilab?");
    answ = messagebox(msg, gettext("Quit Scilab"), "question", [gettext("Yes") gettext("No")], "modal");
    if answ==1 then
        exit;
    end
endfunction


page=list(); lw=6,rw=4;
page($+1)=list(list(lw,'text','Pop-up menu'),...
                list(rw,'pushbutton','Display selection','callback',"act_selected_colors(1)"));
page($+1)=list(list(lw,'popupmenu','item 1|item 2|item 3|item 4',"Callback","popupmenu_callback()","Tag","popupmenu_demo"),...
                list(rw,'pushbutton','Delete selection','callback',"act_selected_colors(2)"));
initial_colors = strcat([gettext("red") gettext("green") gettext("blue") gettext("yellow")], "|"); // Listbox used to display color list
page($+1)=list(list([lw,4],'frame','Slider demo','backgroundcolor',[.8 .8 .8]),...
                list([rw,9],'listbox',initial_colors,'max',30,'tag','colors_listbox',"BackgroundColor", [1 1 1]));
page($+1)=list(list([lw,2],'slider','','min',0,'max',100,'value',50,"SliderStep",[2 10],"Tag", "demo_slider","Callback", "slider_update()"),...
                list(rw));
page($+1)=list(list(1));
page($+1)=list(list(lw,'text','Slider value: 50','fontweight','bold','backgroundcolor',[1 1 1],"HorizontalAlignment", "center","Tag", "slider_text"),...
                list(rw));                
page($+1)=list(list(1));
page($+1)=list(list([lw,4],'frame','Colors list edition','backgroundcolor',[.8 .8 .8]),...
                list(rw));
page($+1)=list(list(lw,'edit','<Enter color name here>',"Tag", "colors_edit","BackgroundColor", [1 1 1]),...
                list(rw));
page($+1)=list(list(lw,'pushbutton','Add color in list box',"callback", "act_color_in_list(1)"),...
                list(rw));
page($+1)=list(list(lw,'pushbutton','Delete color in listbox',"callback", "act_color_in_list(2)"),...
                list(rw));
page($+1)=list(list(1));
page($+1)=list(list(lw-2,'pushbutton',gettext("Default"),"Callback", "reset_uicontrols_demo();"),list(2),...
                list(rw,'pushbutton',gettext("Quit demonstration"),"Callback", "close_uicontrols_demo()"));
guimaker(page,list('Uicontrols 1 demo'),list('fontsize',11),2);
set(gcf(),"Tag", "uicontrols_demo_figure");

menus=list();
menus($+1)=list('Application',list('Exit figure','close(gcf())'),list('Quit Scilab',"exit_scilab();"));
menus($+1)=list('Scilab Graphics',list('Launch plot3d','scf();plot3d()'),list('Launch plot2d','scf();plot2d()'));
menus($+1)=list('Menu',list('Sub menu 1',"menu_callback()"),...
                        list('Sub menu 2',list('Sub menu 2-1',"menu_callback()"),...
                                          list('Sub menu 2-2',"menu_callback()")),...
                        list('Sub menu 3',"checked_menu_callback()"));
menumaker(menus);
demo_viewCode("Uicontrols 1 guimaker.dem.sce");
