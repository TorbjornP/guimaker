mode(1)
// Another example from http://wiki.scilab.org/howto/guicontrol
pg=list();
pg($+1)=list(list('text','exposure time','Horizontalalignment','center'));
pg($+1)=list(list('slider','','Min',0,'Max',100,'Value',5));
pg($+1)=list(list('text','arevscale','Horizontalalignment','center'));
pg($+1)=list(list('slider','','Min',0,'Max',100,'Value',50));
pg($+1)=list(list([2 2],'pushbutton','STOP','callback','OK=%t'),list(2,'radiobutton','bin x2'));
pg($+1)=list(list(2),list(2,'radiobutton','free/trig'));
[hexp,hbri,hbin,htrig]=guimaker(pg,list('objfigure1',220,[],[10 10]))
demo_viewCode("Example 2.dem.sce");
