mode(-1)

// gui example 1
pg=list();
pg($+1)=list(list('pushbutton','Quit','callback','close(gcf())'),..
              list('popupmenu','popup|item1|item2|item3|toto|truc|bidule','tag','popm'),..
              list('pushbutton','num item','callback','cb(''''pb_num'''')'));
pg($+1)=list(list(1.5,'slider','','Min',0,'Max',100,'Value',50,'tag','slider','callback','cb(''''slider'''')'),..
            list(0.5,'edit','50','tag','slider_edit','callback','cb(''''sl_txt'''')'),..
            list([1 6],'listbox','rouge|vert|tomate|chevre|Truc','tag','listbx','backgroundcolor',[1 1 1]));
pg($+1)=list(list([2 5],'frame','an entry box'),list(1));
pg($+1)=list();
pg($+1)=list(list(2,'edit','hello','backgroundcolor',[1 1 1],'tag','new_item'),list(1));
pg($+1)=list();
pg($+1)=list(list(0.5),list(1,'pushbutton','add in list','callback','cb(''''pb_add'''')'),list(1.5));

function cb(cmd)
  // callback function for gui example 1
  h_sl=findobj('tag','slider');
  h_sl_txt=findobj('tag','slider_edit');
  h_new=findobj('tag','new_item');
  h_lb=findobj('tag','listbx');
  select cmd
    case "slider",
      set(h_sl_txt,'string',string(get(h_sl,'value')));
    case "sl_txt",
      set(h_sl,'value',evstr(get(h_sl_txt,'string')));
    case "pb_num"
      
    case "pb_add"
      set(h_lb,'string',[get(h_lb,'string'),get(h_new,'string')]);
  end
endfunction

h=guimaker(pg,list('Uicontrols demo',350),[],2)
demo_viewCode("callback_example_1.dem.sce");
